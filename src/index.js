import React, {Component} from 'react';
import {render} from 'react-dom';
import Process from './Process';
import DockerComposeProcess from './DockerComposeProcess';
import Zone from './Zone'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSkull, faPlay, faDesktop, faList} from '@fortawesome/free-solid-svg-icons'
import './App.css';
import {
  startProcess,
  stopProcess,
  apply,
  dockerCompose,
  loadProcesses,
  loadZones,
  loadDockerPrcesses
} from './lib'


class App extends Component {

  constructor() {
    super();
    this.state = { processes: [], zone: [], dockerComposeProcesses: [] }
  }

  componentDidMount() {
    this.loadProcesses()
    this.loadZones()
    this.loadDockerPrcesses()
    this.requestPermissions()
  }

  componentWillUpdate(nextProps, nextState, nextContext) {
    const { processes } = this.state
    const { processes: nextProcesses } = nextState
    if (!nextProcesses) {
      return
    }

    const message = processes
      .filter(process => {
        const nextProcess = nextProcesses.find(it => it.name === process.name)
        if (!nextProcess) {
          return false
        }

        return nextProcess && nextProcess.status === 'stopped' && process.status !== 'stopped'
      })
      .map(process => `Process ${process.name} stopped!`)
      .join('\n')

    this.notify(message)
    
    
  }

  render() {
    const { zone } = this.state

    return (
      <div>
        <ul>
          { this.renderProcesses() }
          { this.renderDockerProcesses() }
          { false && this.renderApplyRow() }

          <li className='dnsRecord'>
            <div className='recordType'>type</div>
            <div className='domain'>domain</div>
            <div className='target'>target</div>
            <div className='port'>port</div>
          </li>
          { zone && zone.map((zone, index) => <Zone key={`${zone.domain}_${index}`} zone={zone}/>) }
        </ul>

      </div>
    );
  }

  renderProcesses() {
    const { processes } = this.state

    return (
      <React.Fragment>
        <li className='header'>
          <div className='fugeId'>Fuge</div>
          <div className='processName'>Process Name</div>
          <div className='processType'>type</div>
          <div className='processStatus'>Status</div>
          <div className='monitor'><FontAwesomeIcon icon={faDesktop}/></div>
          <div className='monitor'><FontAwesomeIcon icon={faList}/></div>
          <div>Actions</div>
        </li>
        { processes.map((process, index) => <Process
            key={`process_${process.name}_${index}`}
            process={process}
            enableNotification={ this.enableNotification }
          />)
        }
        { this.renderAllProcessesRow() }
      </React.Fragment>
    )
  }

  renderAllProcessesRow() {
    return (
      <li>
        <div className='allProcessesName'>all processes</div>
        <div className='processStatus'></div>
        <FontAwesomeIcon className='action play' icon={faPlay} onClick={() => startProcess()}/>
        <FontAwesomeIcon className='action kill' icon={faSkull} onClick={() => stopProcess()}/>
      </li>
    )
  }


  renderDockerProcesses() {
    const { dockerComposeProcesses } = this.state
    return (
      <React.Fragment>
        <li className='header'>
          <div className='processName'>Process Name</div>
          <div className='processType'>Command</div>
          <div className='processStatus'>State</div>
          <div className='processStatus'>ports</div>
        </li>
        { dockerComposeProcesses.map((process, index)  => <DockerComposeProcess key={`${process.name}_${index}`} process={process}/>) }
        { false && this.renderDockerComposeRow() }
      </React.Fragment>
    )
  }


  renderApplyRow() {
    return (
      <li>
        <div className='applyPrompt'>apply></div>
        <div className='applyInput'>
          <input type='text' onKeyDown={this.handleApplyKeyDown}/>
        </div>
      </li>
    )
  }

  renderDockerComposeRow() {
    return (
      <li>
        <div className='applyPrompt'>docker-compose></div>
        <div className='applyInput'>
          <input type='text' onKeyDown={this.handleDockerComposeKeyDown}/>
        </div>
      </li>
    )
  }

  handleApplyKeyDown = (event) => {
    if (event.keyCode === 13) {
      apply(event.target.value)
    }
  }

  handleDockerComposeKeyDown = (event) => {
    if (event.keyCode === 13) {
      dockerCompose(event.target.value)
    }
  }

  loadProcesses = async () => {
    const processes = (await loadProcesses())
      .sort((processA, processB) => {
        const nameA = `${processA.fugeId}_${processA.name}`
        const nameB = `${processB.fugeId}_${processB.name}`
        if (nameA < nameB) return -1
        if (nameA > nameB) return 1
        return 0
      })
      .filter(process => process.type === 'process')

    this.setState({...this.state, processes})
    setTimeout(this.loadProcesses, 1000)
  }

  loadZones = async () => {
    const zone = await loadZones()
    this.setState({...this.state, zone})
    setTimeout(this.loadZones, 10000)
  }


  loadDockerPrcesses = async () => {
    const dockerComposeProcesses = await loadDockerPrcesses()
    this.setState({...this.state, dockerComposeProcesses})
    setTimeout(this.loadDockerPrcesses, 5000)
  }

  enableNotification = (processName) => {
    const process = this.state.processes.find(it => it.name === processName)
    if (!process) {
      return
    }

    process.notificationEnabled = !process.notificationEnabled

    this.forceUpdate()
  }

  requestPermissions() {
    if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) { });
    }
  }

  notify(message) {
    if (Notification.permission === 'granted') {
     new Notification(message);
    }
  }




}

render(<App/>, document.getElementById('root'));
