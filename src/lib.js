const options = {method: "post"}

const BASE_URL = window.location.origin.replace(3001, 3000)

export const startProcess = (process = 'all') => {
  console.log(window.location)
  fetch(`${BASE_URL}/api/commands/start?args[]=${process}`, options)
    .catch(error => console.log('Fetch error'))
}

export const stopProcess = (process = 'all') => {
  fetch(`${BASE_URL}/api/commands/stop?args[]=${process}`, options)
    .catch(error => console.log('Fetch error'))
}

export const debugProcess = (name) => {
  fetch(`${BASE_URL}/api/commands/debug?args[]=${name}`, options)
    .catch(error => console.log('Fetch error'))
}

export const restartProcess = (name) => {
  fetch(`${BASE_URL}/api/commands/restart?args[]=${name}`, options)
    .catch(error => console.log('Fetch error'))
}

export const apply = (args) => {
  fetch(`${BASE_URL}/api/commands/apply?${argsToQueryString(args)}`, options)
    .catch(error => console.log('Fetch error'))
}

export const dockerCompose = (args) => {
  fetch(`${BASE_URL}/api/commands/docker-compose?${argsToQueryString(args)}`, options)
    .catch(error => console.log('Fetch error'))
}

export const loadProcesses = async () => {
  try {
    const result = await fetch(`${BASE_URL}/api/commands/ps`, {method: "post"})
    return await result.json()
  } catch(error) {
    return []
  }
}

export const loadDockerPrcesses = async  (args) => {
  try {
    const result = await fetch(`${BASE_URL}/api/commands/docker-compose/ps`, options)
    return await result.json()
  } catch (error) {
    return []
  }
}

export const loadZones = async () => {
  try {
    const result = await fetch(`${BASE_URL}/api/commands/zone`, {method: "post"})
    return await result.json()
  } catch (error) {
    return []
  }
}

function argsToQueryString(args) {
  return args
    .split(' ')
    .map(arg => `args[]=${arg}`)
    .join('&')
}