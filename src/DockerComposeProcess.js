import React, { Component } from 'react';

export default class DockerComposeProcess extends Component {

    render() {
        const { process: { name, process, status, tcpStatus } } = this.props

        return (
            <li className={`dockerComposeProcess ${status}`}>
                <div className='processName'>{ name }</div>
                <div className='process'>{ process }</div>
                <div className='status'>{ status }</div>
                <div className='tcpStatus'>{ tcpStatus }</div>
            </li>
        )
    }

}
