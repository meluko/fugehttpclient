import React, { Component } from 'react';

export default class Zone extends Component {
    render() {
        const { zone: { type, domain, target, port } } = this.props

        return (
            <li className='dnsRecord'>
                <div className='recordType'>{ type }</div>
                <div className='domain'>{ domain }</div>
                <div className='target'>{ target }</div>
                <div className='port'>{ port || '-' }</div>
            </li>
        )
    }
}
