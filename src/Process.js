import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSkull, faRedo, faBug, faPlay, faDesktop, faList, faBell } from '@fortawesome/free-solid-svg-icons'
import {
    debugProcess,
    startProcess,
    stopProcess,
    restartProcess
} from './lib'

export default class Process extends Component {

    render() {
        const { process: { fugeId, name, type, monitor, tail, status, notificationEnabled }, enableNotification } = this.props
        const className = status

        return (
            <li className={ className }>
                <div className='fugeId'>{ fugeId }</div>
                <div className='processName'>{ name }</div>
                <div className='processType'>{ type }</div>
                <div className='processStatus'>{ status }</div>
                <div className={ `monitor ${monitor}` }><FontAwesomeIcon icon={faDesktop} onClick={ () => {} }  /></div>
                <div className={ `tail ${tail}` }><FontAwesomeIcon icon={faList} onClick={ () => {} }  /></div>
                <div className='actions'>
                  <FontAwesomeIcon className='action debug' icon={faBug} onClick={ () => debugProcess(name) }  />
                  { status === 'running'
                    ? <FontAwesomeIcon className='action kill' icon={faSkull} onClick={ () => stopProcess(name) } />
                    : <FontAwesomeIcon className='action play' icon={faPlay} onClick={ () => startProcess(name) } />
                  }
                  { status === 'running' &&
                  <FontAwesomeIcon className='action restart' icon={faRedo}  onClick={ () => restartProcess(name) } />
                  }
                  <FontAwesomeIcon className={`action notification ${notificationEnabled ? 'enabled' : '' }`} icon={faBell} onClick={ () => enableNotification(name) } />
                </div>

            </li>
        )
    }

}
